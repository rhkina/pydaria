# Arquitetura para projetos Python

Baseado no excelente vídeo do [Bruno Rocha](https://youtu.be/-qWySnuoaTM).

## Instalando as dependências
```bash
$ pip install -r requirements.txt
```

## Para ver os comandos criados
```bash
$ flask --help
```

## Criando o banco de dados com os produtos
```bash
$ flask create-db
```

## Acrescentando alguns produtos no banco de dados
```bash
$ flask populate-db
```

## Limpando o banco de dados (Products e Users)
```bash
$ flask drop-db
```

## Criando usuários (preencha *`username`* e *`password`* com seu usuário e senha, respectivamente)
```bash
$ flask add-user -u <username> -p <password>
```
