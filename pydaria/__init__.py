from flask import Flask
from pydaria.ext.configuration import AddConfiguration
from pydaria.ext.appearance import AddAppearance
from pydaria.ext.database import AddDatabase
from pydaria.ext.auth import AddAuthentication
from pydaria.ext.admin import AddAdministration
from pydaria.ext.commands import AddCommands
from pydaria.blueprints.webui.views import AddViews
from pydaria.blueprints.restapi.apis import AddAPIs

def create_app(test_config=None):

    app = Flask(__name__)

    print('Application created...')

    AddConfiguration(app)
    AddAppearance(app)
    AddDatabase(app)
    AddAuthentication(app)
    AddAdministration(app)
    AddCommands(app)
    AddViews(app)
    AddAPIs(app)

    print('Application ready!')

    return app