from flask import abort, jsonify, Blueprint, url_for
from flask_restx import Resource, Api

from pydaria.models import Products


class AddAPIs(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        bp = Blueprint("restapi", __name__, url_prefix="/api/v1")
        api = Api(bp, version='1.0', title='Pydaria API',
            description='You are in the Pydaria API documentation!'
        )

        @api.route('/products/')
        class ProductsResource(Resource):
            def get(self):
                products = Products.query.all() or abort(204)
                return jsonify(
                    {"products": [product.to_dict() for product in products]}
                )

        @api.route('/products/<product_id>')
        class ProductItemResource(Resource):
            def get(self, product_id):
                product = Products.query.filter_by(id=product_id).first() or abort(404)
                return jsonify(product.to_dict())

        app.register_blueprint(bp)

        print('APIs added...')

        return app