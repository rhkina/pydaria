from flask import Flask, render_template, abort
from flask import Blueprint
from pydaria.models import Products


class AddViews(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def index(self):
        products = Products.query.all()
        return render_template('index.html', products=products)

    def product(self, product_id):
        product = Products.query.filter_by(id=product_id).first() or abort(
            404, "produto não encontrado"
        )
        return render_template("product.html", product=product)

    def init_app(self, app):
        print('Views added...')
        bp = Blueprint("webui", __name__, template_folder="templates")

        bp.add_url_rule("/", view_func=self.index)
        bp.add_url_rule(
            "/product/<product_id>", view_func=self.product, endpoint="productview"
        )
        app.register_blueprint(bp)

