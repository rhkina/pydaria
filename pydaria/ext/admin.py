from flask_admin import Admin
from flask_admin.base import AdminIndexView
from flask_admin.contrib import sqla
from flask_simplelogin import login_required

from pydaria.models import Products, Users
from pydaria.ext.database import db


class AddAdministration(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        # Proteger o admin com login via Monkey Patch
        AdminIndexView._handle_view = login_required(AdminIndexView._handle_view)
        sqla.ModelView._handle_view = login_required(sqla.ModelView._handle_view)
        admin = Admin(app, name='pydaria', template_mode='bootstrap3')
        admin.add_view(sqla.ModelView(Products, db.session))
        admin.add_view(sqla.ModelView(Users, db.session))
        print('Administration (flask_admin) added...')
