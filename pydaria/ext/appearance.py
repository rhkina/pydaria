from flask_bootstrap import Bootstrap

class AddAppearance(object):

    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        Bootstrap(app)
        print('Bootstrap added...')
