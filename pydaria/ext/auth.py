from flask_simplelogin import SimpleLogin
from werkzeug.security import check_password_hash, generate_password_hash
from pydaria.models import Users
from .database import db


def verify_login(user):
    """Valida o usuario e senha para efetuar o login"""
    username = user.get('username')
    password = user.get('password')
    if not username or not password:
        return False
    existing_user = Users.query.filter_by(username=username).first()
    if not existing_user:
        return False
    if check_password_hash(existing_user.password, password):
        return True
    return False


def create_user(username, password):
    """Registra um novo usuario caso nao esteja cadastrado"""
    if Users.query.filter_by(username=username).first():
        raise RuntimeError(f'{username} ja esta cadastrado')
    user = Users(username=username, password=generate_password_hash(password))
    db.session.add(user)
    db.session.commit()
    return user

class AddAuthentication(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        SimpleLogin(app, login_checker=verify_login)
        print('Authentication added...')
