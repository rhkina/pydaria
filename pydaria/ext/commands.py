import click
from pydaria.models import Products, Users
from pydaria.ext.database import db

from pydaria.ext.auth import create_user

def create_db():
    """Creates database"""
    db.create_all()


def drop_db():
    """Cleans database"""
    db.drop_all()


def populate_db():
    """Populate db with sample data"""
    data = [
        Products(
            id=1, name="Ciabatta", price="10", description="Italian Bread"
        ),
        Products(id=2, name="Baguete", price="15", description="French Bread"),
        Products(id=3, name="Pretzel", price="20", description="German Bread"),
    ]
    db.session.bulk_save_objects(data)
    db.session.commit()
    return Products.query.all()

class AddCommands(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        # add multiple commands in a bulk
        for command in [create_db, drop_db, populate_db]:
            app.cli.add_command(app.cli.command()(command))

        # add a single command
        @app.cli.command()
        @click.option('--username', '-u')
        @click.option('--password', '-p')
        def add_user(username, password):
            """Adds a new user to the database"""
            return create_user(username, password)

        print('Commands added...')
