from dynaconf import FlaskDynaconf

class AddConfiguration(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        FlaskDynaconf(app)
        print('Configurations added...')
