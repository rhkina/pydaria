from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


db = SQLAlchemy()

class AddDatabase(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        migrate = Migrate(app, db)

        print('Database initialized...')
        return (db.init_app(app))